## Interface: 80000
## Title: ZugZug Guild Money (Broker Plugin)
## Notes: Sums up your gained guild money
## Author: Darool (Guild: Zug Zug / Realm: Quel'Thalas EU)
## Version: 8.0.0.0
## X-Category: Auction & Economy, Guild
## SavedVariables: ZZGMDB
## X-Embeds: LibStub, CallbackHandler, LibDataBroker
## DefaultState: Enabled
## LoadOnDemand: 0
## LoadManagers: AddonLoader

#@no-lib-strip@
libs\LibStub\LibStub.lua
libs\CallbackHandler-1.0\CallbackHandler-1.0.xml
libs\LibDataBroker-1.1\LibDataBroker-1.1.lua
#@end-no-lib-strip@

# Core
zzgm.lua
