local addonName = ...
local guildName = nil
local db = nil
local guildMaster = nil
local pName = UnitName("player")
local moneyFrames = {} -- Chat frames that want the money prints.
local f = CreateFrame("Frame")
local prefix = "zzgm"
local multiplier = nil
local addonVersion = "8.0.0.0"

local today = nil
local sessionTotal = 0
local allTimeTotal = nil
local lastTransmitTime = nil

local guildColor = "|cff40ff40"
local sysColor = "|cffffff00"

------ Locale
local L_NAME = "ZugZug Guild Money"
local L_SESSION = "This session"
local L_WEEK = "Last 7 days"
local L_MONTH = "Last 30 days"
local L_ALLTIME = "All time total"
local L_NOINCOME = "You haven't generated any income to your guild for the past month - stop slacking!"
local L_NOINCOME_GM = "You haven't generated any income to your guild for the past month - stop slacking! %s is very sad."
local L_GOODBOY = "Nice you are finally helping the guild!"
local L_GOODBOY_GM = "Nice you are finally helping the guild, thanks for that!"
local L_MAINCHAR = "%s Your main character is %s."
local L_TOPFIVE = "Top 5 contributors"
local L_SET_HEADER = "Main character?"
local L_SET_BODY = "Please set which character in %s<%s>|r is your main, so that when you broadcast how much gold you earned to the guild, they see all your efforts combined. If you don't specify a name you will be reminded again later."
local L_SET_SET = "Set main"
local L_SET_LATER = "Do it later"
local L_SET_MESSAGE = "%sYour main character for |r%s<%s>|r%s has been set to %q. If you want to change it later, use the /zzgm command.|r"
local L_SET_NOTSET = "%sYou need to set your main character if you want to broadcast your earnings to the rest of the guild. Use /zzgm to do so at a later point. You will be reminded again next session.|r"
local L_NOT_LOADED = "You are not in a guild, or there is some error"
------

local queryForMainCharacter
do
	local window = nil
	local function closeNoSave()
		if not db.mainChar then
			print(L_SET_NOTSET:format(sysColor))
		end
		window:Hide()
	end
	function queryForMainCharacter()
		if window then
			window:Show()
			return
		end
		window = CreateFrame("Frame", nil, UIParent)
		window:SetWidth(260)
		window:SetHeight(200)
		window:SetPoint("CENTER")
		window:SetBackdrop({
			bgFile = "Interface\\Tooltips\\UI-Tooltip-Background",
			edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border",
			tile = true, tileSize = 16, edgeSize = 16,
			insets = { left = 4, right = 4, top = 4, bottom = 4 }
		})
		window:SetBackdropColor(0, 0, 0, .75)

		local header = window:CreateFontString(nil, "ARTWORK", "GameFontNormalLarge")
		header:SetPoint("TOPLEFT", 12, -12)
		header:SetPoint("TOPRIGHT", -12, -12)
		header:SetText(L_SET_HEADER)

		local later = CreateFrame("Button", "ZZGMSetLater", window, "UIPanelButtonTemplate")
		later:SetHeight(32)
		later:SetWidth(100)
		later:SetText(L_SET_LATER)
		later:SetPoint("BOTTOMLEFT", window, "BOTTOMLEFT", 12, 12)
		later:SetScript("OnClick", closeNoSave)

		local editbox = nil

		local set = CreateFrame("Button", "ZZGMSetMainChar", window, "UIPanelButtonTemplate")
		set:SetPoint("BOTTOMRIGHT", window, "BOTTOMRIGHT", -12, 12)
		set:SetHeight(32)
		set:SetWidth(100)
		set:SetScript("OnClick", function()
			local input = editbox:GetText()
			if not input or input:trim():len() < 3 then
				closeNoSave()
			else
				db.mainChar = input
				print(L_SET_MESSAGE:format(sysColor, guildColor, guildName, sysColor, db.mainChar))
				window:Hide()
			end
		end)
		set:SetText(L_SET_SET)

		editbox = CreateFrame("EditBox", nil, window, "InputBoxTemplate")
		editbox:SetPoint("LEFT", later, "LEFT", 14, 0)
		editbox:SetPoint("RIGHT", set, "RIGHT", -10, 0)
		editbox:SetPoint("BOTTOM", set, "TOP", 0, 8)
		editbox:SetHeight(20)
		editbox:SetText(db.mainChar or pName)
		editbox:SetAutoFocus(false)
		editbox:SetMaxLetters(12)
		editbox:SetScript("OnEnterPressed", function()
			set:GetScript("OnClick")()
		end)

		local text = window:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
		text:SetPoint("TOPLEFT", header, "BOTTOMLEFT", 4, -4)
		text:SetPoint("BOTTOMRIGHT", editbox, "TOPRIGHT", 4, 4)
		text:SetJustifyH("LEFT")
		text:SetJustifyV("TOP")
		text:SetText(L_SET_BODY:format(guildColor, guildName))

		window:Show()
	end
end

local function sortOnTotal(a, b)
	local aT = a == db.mainChar and allTimeTotal or db.others[a]
	local bT = b == db.mainChar and allTimeTotal or db.others[b]
	return aT > bT
end

local tmpSort = {}
local scoreFormat = "%d. %s"
local function getHint(added)
	local hint = guildMaster and L_NOINCOME_GM:format(guildMaster) or L_NOINCOME
	if added then
		hint = guildMaster and L_GOODBOY_GM:format(guildMaster) or L_GOODBOY
	end
	if db.mainChar then return L_MAINCHAR:format(hint, db.mainChar) end
	return hint
end
local function updateTooltip(tt)
	if not db then
		tt:AddLine(L_NOT_LOADED, 1, .2, .2, 1)
		return
	end

	tt:AddLine(L_NAME)
	local added = nil
	if sessionTotal > 0 then
		tt:AddDoubleLine(L_SESSION, GetCoinTextureString(sessionTotal), 1, 1, 1, 0, 1, 0)
		added = true
	end
	if db.days[today] and db.days[today] > 0 then
		tt:AddDoubleLine(HONOR_TODAY, GetCoinTextureString(db.days[today]), 1, 1, 1, 0, 1, 0)
		added = true
	end
	local week = 0
	for i = today, (today - 7), -1 do
		week = week + (db.days[i] or 0)
	end
	if week > 0 then
		tt:AddDoubleLine(L_WEEK, GetCoinTextureString(week), 1, 1, 1, 0, 1, 0)
		added = true
	end
	local month = week
	for i = (today - 8), (today - 30), -1 do
		month = month + (db.days[i] or 0)
	end
	if month > 0 then
		tt:AddDoubleLine(L_MONTH, GetCoinTextureString(month), 1, 1, 1, 0, 1, 0)
		added = true
	end
	if allTimeTotal and allTimeTotal > 0 then
		tt:AddDoubleLine(L_ALLTIME, GetCoinTextureString(allTimeTotal), 1, 1, 1, 0, 1, 0)
		added = true
	end

	if db.mainChar then
		tmpSort[1] = db.mainChar
	end
	for k, v in pairs(db.others) do
		tmpSort[#tmpSort + 1] = k
	end
	if #tmpSort > 0 then
		added = true
		table.sort(tmpSort, sortOnTotal)
		tt:AddLine(" ")
		tt:AddLine(L_TOPFIVE)
		for i = 1, 5 do
			local p = tmpSort[i]
			if p then
				local v = p == db.mainChar and allTimeTotal or db.others[p]
				tt:AddDoubleLine(scoreFormat:format(i, p), GetCoinTextureString(v), 1, 1, 1, 0, 1, 0)
			else
				break
			end
		end
		wipe(tmpSort)
	end
	tt:AddLine(" ")
	tt:AddLine(getHint(added), 0.2, 1, 0.2, 1)
end

local bZZGM = LibStub("LibDataBroker-1.1"):NewDataObject("ZZGM", {
	type = "data source",
	icon = "Interface\\MoneyFrame\\UI-GoldIcon",
	text = L_NAME,
	OnTooltipShow = updateTooltip,
})

local function updateText()
	local amount = db.days[today] or 0
	if amount == 0 then
		bZZGM.text = L_NAME
	else
		bZZGM.text = format("Collected today: %s", GetCoinTextureString(amount))
	end
end

local matchGold = GOLD_AMOUNT:gsub("%%d", "(%%d+)")
local matchSilver = SILVER_AMOUNT:gsub("%%d", "(%%d+)")
local matchCopper = COPPER_AMOUNT:gsub("%%d", "(%%d+)")
local function getTotalFromString(msg)
	local g = (msg:match(matchGold) or 0) * 10000
	local s = (msg:match(matchSilver) or 0) * 100
	local c = msg:match(matchCopper) or 0
	return g + s + c
end

local function scanForGM()
	if guildMaster then return end
	for i = 1, GetNumGuildMembers(true) do
		local n, _, r = GetGuildRosterInfo(i)
		if n and r == 0 then
			if n ~= pName then guildMaster = n end
			break
		end
	end
end

local haveWarnedThisSession = nil
local function broadcast(value)
	if not db.mainChar then
		if not haveWarnedThisSession then
			queryForMainCharacter()
			haveWarnedThisSession = true
		end
		return
	end
	-- protocolversion#mainchar#total
	SendAddonMessage(prefix, "1#" .. db.mainChar .. "#" .. tostring(value), "GUILD")
end

local function setup()
	ZZGMDB = ZZGMDB or {}
	if type(ZZGMDB[guildName]) ~= "table" then
		ZZGMDB[guildName] = {}
	end
	db = ZZGMDB[guildName]
	for k, v in pairs({
		days = {}, -- Last 30 days, per-day generated income
		total = 0, -- Total value beyond the past 30 days is added up here
		others = {}, -- player:total values from the rest of the guild
		mainChar = nil, -- The name we broadcast to others
		lastReminder = nil, -- Which day we reminded about missing main char
	}) do
		if type(db[k]) == "nil" then
			db[k] = v
		end
	end

	local c = ChatTypeInfo.GUILD
	if c then
		guildColor = ("|cff%02x%02x%02x"):format(c.r * 255, c.g * 255, c.b * 255)
	end
	c = ChatTypeInfo.SYSTEM
	if c then
		sysColor = ("|cff%02x%02x%02x"):format(c.r * 255, c.g * 255, c.b * 255)
	end

	-- Borrowed from MoneyFu (probably ckk)
	local serverHour, serverMinute = GetGameTime()
	local utcHour = tonumber(date("!%H"))
	local utcMinute = tonumber(date("!%M"))
	local ser = serverHour + serverMinute / 60
	local utc = utcHour + utcMinute / 60
	local offset = floor((ser - utc) * 2 + 0.5) / 2
	if offset >= 12 then
		offset = offset - 24
	elseif offset < -12 then
		offset = offset + 24
	end
	today = floor((time() / 60 / 60 + offset) / 24)

	-- Anything older than 30 days is added to the total and reset
	local oneMonth = today - 30
	allTimeTotal = db.total
	for k, v in pairs(db.days) do
		allTimeTotal = allTimeTotal + v
		if k < oneMonth then
			db.total = db.total + v
			db.days[k] = nil
		end
	end
	if allTimeTotal > 0 then
		broadcast(allTimeTotal)
	end

	_G.SlashCmdList.ZZGMSlash = queryForMainCharacter
	_G.SLASH_ZZGMSlash1 = "/zzgm"

	f:UPDATE_CHAT_WINDOWS()

	scanForGM()
	updateText()

	-- Figure out which rank of Cash Flow we have
	local l = GetGuildLevel()
	if l > 15 then
		multiplier = 10
	elseif l > 4 then
		multiplier = 5
	else
		multiplier = nil
	end

	setup = nil
end

do
	local function findMoneyEvent(...)
		for i = 1, select("#", ...) do
			if (select(i, ...)) == "MONEY" then return true end
		end
	end
	function f:UPDATE_CHAT_WINDOWS()
		-- Find out which chat frames want the CHAT_MSG_MONEY event
		-- Not sure if this is the best way, but it works
		wipe(moneyFrames)
		for i, name in next, CHAT_FRAMES do
			if findMoneyEvent(GetChatWindowMessages(i)) then
				moneyFrames[#moneyFrames + 1] = _G[name]
			end
		end
	end
end

do
	local function attemptToSetup()
		if guildName then return true end
		guildName = GetGuildInfo("player")
		if not guildName then return end
		f:UnregisterEvent("PLAYER_LOGIN")
		f:UnregisterEvent("PLAYER_ENTERING_WORLD")
		if setup then setup() end
		return true
	end

	function f:PLAYER_LOGIN()
		if not attemptToSetup() then
			self:RegisterEvent("PLAYER_ENTERING_WORLD")
		end
		GuildRoster()
		if not IsAddonMessagePrefixRegistered(prefix) then
			RegisterAddonMessagePrefix(prefix)
		end
	end
	f.PLAYER_ENTERING_WORLD = f.PLAYER_LOGIN

	function f:GUILD_ROSTER_UPDATE()
		attemptToSetup()
		scanForGM()
		if guildMaster and guildName then
			self:UnregisterEvent("GUILD_ROSTER_UPDATE")
		end
	end
end

function f:CHAT_MSG_ADDON(addon, message, channel, sender)
	if addon ~= prefix or sender == pName then return end
	-- Apparently CHAT_MSG_ADDON can arrive before PLAYER_LOGIN, so lets go.
	if not db then return end
	local version, player, total = strsplit("#", message)
	if tonumber(version) ~= 1 or not player or not total then return end
	local v = tonumber(total)
	if not v or v < 1 then return end
	db.others[player] = v
end

do
	local personalMoney = YOU_LOOT_MONEY:gsub("%%s", "(.+)")
	local splitMoney = LOOT_MONEY_SPLIT:gsub("%%s", "(.+)")
	local withDot = YOU_LOOT_MONEY .. "."
	local function moneyOutput(msg)
		local c = ChatTypeInfo.MONEY
		for i, frame in next, moneyFrames do
			frame:AddMessage(msg, c.r, c.g, c.b, c.id)
		end
	end
	function f:CHAT_MSG_MONEY(msg)
		-- |not db| catches failed startup
		if not db or not multiplier then return end
		local money = msg:match(personalMoney)
		if not money then money = msg:match(splitMoney) end
		if money then
			local looted = (getTotalFromString(money) or 0)
			local guildShare = looted * multiplier / 100

			sessionTotal = sessionTotal + guildShare
			db.days[today] = (db.days[today] or 0) + guildShare
			allTimeTotal = allTimeTotal + guildShare

			local t = GetTime()
			if not lastTransmitTime or (t > lastTransmitTime + 120) then
				broadcast(allTimeTotal)
				lastTransmitTime = t
			end

			updateText()

			local text = withDot:format(GetCoinTextureString(looted))
			moneyOutput(text)
		else
			moneyOutput(msg)
		end
	end
	function f:CHAT_MSG_CURRENCY(...)
		print("CHAT_MSG_CURRENCY", tostringall(...))
	end
end

f:RegisterEvent("UPDATE_CHAT_WINDOWS")
f:RegisterEvent("PLAYER_LOGIN")
f:RegisterEvent("GUILD_ROSTER_UPDATE")
f:RegisterEvent("CHAT_MSG_ADDON")
f:RegisterEvent("CHAT_MSG_MONEY")
--f:RegisterEvent("CHAT_MSG_CURRENCY")
f:SetScript("OnEvent", function(self, event, ...) self[event](self, ...) end)

local function filter() return true end
ChatFrame_AddMessageEventFilter("CHAT_MSG_MONEY", filter)
